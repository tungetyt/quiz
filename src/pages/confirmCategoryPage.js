import { useState } from "react";

import Culture from "../elements/Culture";
import History from "../elements/History";
import Moto from "../elements/Moto";
import Programming from "../elements/Programming";
import Technology from "../elements/Technology";
import QuestionPage from "../pages/questionPage";

import backBtn from "../assets/cofnij_x.svg";
import exitBtn from "../assets/zamknij_x.svg";
import startCulture from "../assets/culture_start.png";
import startHistory from "../assets/history_start.png";
import startMoto from "../assets/moto_start.png";
import startProgramming from "../assets/programming_start.png";
import startTechnology from "../assets/button_strzałka_plus.svg";
import q from "../assets/logo_q.png";

const ConfirmCategoryPage = ({ chosenCategory, setChosenCategory }) => {
  const [confirmedCategory, setConfirmedCategory] = useState(false);

  const showRightDescription = () => {
    switch (chosenCategory) {
      case "technology":
        return <Technology />;
      case "moto":
        return <Moto />;
      case "history":
        return <History />;
      case "culture":
        return <Culture />;
      case "programming":
        return <Programming />;
      default:
        return;
    }
  };

  const setBackground = () => {
    if (chosenCategory === "technology" && confirmedCategory) {
      return "technology1 main-content";
    } else if (chosenCategory === "moto" && confirmedCategory) {
      return "moto1 main-content";
    } else if (chosenCategory === "culture" && confirmedCategory) {
      return "culture1 main-content";
    } else if (chosenCategory === "programming" && confirmedCategory) {
      return "programming1 main-content";
    } else if (chosenCategory === "history" && confirmedCategory) {
      return "history1 main-content";
    } else if (chosenCategory === "technology" && confirmedCategory === false) {
      return "technology2  main-content";
    } else if (chosenCategory === "moto" && confirmedCategory === false) {
      return "moto2 main-content";
    } else if (chosenCategory === "culture" && confirmedCategory === false) {
      return "culture2 main-content";
    } else if (chosenCategory === "programming" && confirmedCategory === false) {
      return "programming2 main-content";
    } else if (chosenCategory === "history" && confirmedCategory === false) {
      return "history2 main-content";
    }
  };

  const setImgSrc = () => {
    switch (chosenCategory) {
      case "technology":
        return startTechnology;
      case "moto":
        return startMoto;
      case "history":
        return startHistory;
      case "culture":
        return startCulture;
      case "programming":
        return startProgramming;
      default:
        return;
    }
  };

  const backButton = () => {
    if (chosenCategory && confirmedCategory === false) {
      setChosenCategory("");
    } else if (chosenCategory && confirmedCategory === true) {
      setConfirmedCategory(false);
    }
  };

  return (
    <div className={setBackground()}>
      <div className="header">
        <img src={q} alt="logo" className="q" />
        <img src={backBtn} alt="back" onClick={backButton} />
        <h1>Quiz</h1>
        <img src={exitBtn} alt="exit" onClick={() => setChosenCategory("")} />
        {confirmedCategory ? null : (
          <p className={"chosenCat bar_" + chosenCategory}>
            Wybrana kategoria:
          </p>
        )}
      </div>
      {confirmedCategory ? (
        <QuestionPage
          chosenCategory={chosenCategory}
          setChosenCategory={setChosenCategory}
          confirmedCategory={confirmedCategory}
          setConfirmedCategory={setConfirmedCategory}
          setImgSrc={setImgSrc}
        />
      ) : (
        <div className="confirmCategory">
          {showRightDescription()}
          <label>
            <input
              type="submit"
              value="rozpocznij"
              onClick={() => setConfirmedCategory(true)}
              className={"button_" + chosenCategory}
            />
            <img src={setImgSrc()} alt="start" />
          </label>
        </div>
      )}
    </div>
  );
};

export default ConfirmCategoryPage;
