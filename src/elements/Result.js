import { Droppable } from "react-beautiful-dnd";

import AnswerDND from "./AnswerDND";
import correct from "../assets/poprawna_odpowiedź_.png";

const Result = ({
  initialData,
  chosenCategory,
  questionNumber,
  addStyle,
  chosenAnswer,
}) => {
  const result = initialData[chosenCategory][questionNumber];
  return (
    <Droppable droppableId={result.columns["column-2"].id}>
      {(provided) => (
        <div
          className="result"
          ref={provided.innerRef}
          {...provided.droppableProps}
        >
          {result.columns["column-2"].choicesIds.length === 0 ? (
            <p className="choice">drag here & drop</p>
          ) : null}
          <img
            src={correct}
            alt="correct"
            className={chosenAnswer === result.correct ? "showImg" : "hideImg"}
          />
          {result.columns["column-2"].choicesIds.map((choice, index) => (
            <AnswerDND
              key={choice.charCodeAt(0)}
              choice={choice}
              index={index}
              initialData={initialData}
              chosenCategory={chosenCategory}
              questionNumber={questionNumber}
              addStyle={addStyle}
            />
          ))}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );
};

export default Result;
