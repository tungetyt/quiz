import { DragDropContext } from "react-beautiful-dnd";
import { useState } from "react";

import Column from "../elements/Column";
import Result from "../elements/Result";
import initialData from "../initialData";

import correctCulture from "../assets/correct-culture.png";
import correctHistory from "../assets/correct-history.png";
import correctMoto from "../assets/correct-moto.png";
import correctProgramming from "../assets/correct-programming.png";
import correctTechnology from "../assets/correct-technology.png";

const Answer = ({
  chosenCategory,
  questionNumber,
  setQuestionNumber,
  setCorrectAnswers,
}) => {
  const [chosenAnswer, setChosenAnswer] = useState("");
  const [addStyle, setAddStyle] = useState(false);
  const [state, setState] = useState(initialData);

  const checkAnswer = (correctAnswer, choice) => {
    if (correctAnswer === choice) {
      setCorrectAnswers((prevState) => prevState + 1);
    }
  };

  const afterSeconds = () => {
    setTimeout(() => setQuestionNumber((prevState) => prevState + 1), 350);
    setTimeout(() => setAddStyle(false), 350);
  };

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    const start =
      state[chosenCategory][questionNumber].columns[source.droppableId];
    const finish =
      state[chosenCategory][questionNumber].columns[destination.droppableId];

    if (start === finish) {
      const newChoicesIds = Array.from(start.choicesIds);
      newChoicesIds.splice(source.index, 1);
      newChoicesIds.splice(destination.index, 0, draggableId);

      const newColumn = {
        ...start,
        choicesIds: newChoicesIds,
      };

      const newState = {
        ...state,
        [chosenCategory]: {
          ...state[chosenCategory],
          [questionNumber]: {
            ...state[chosenCategory][questionNumber],
            columns: {
              ...state[chosenCategory][questionNumber].columns,
              [newColumn.id]: newColumn,
            },
          },
        },
      };

      setState(newState);
      return;
    }

    //Moving from one list to another

    const startChoicesIds = Array.from(start.choicesIds);
    startChoicesIds.splice(source.index, 1);

    const newStart = {
      ...start,
      choicesIds: startChoicesIds,
    };

    const finishChoicesIds = Array.from(finish.choicesIds);
    finishChoicesIds.splice(destination.index, 0, draggableId);

    const newFinish = {
      ...finish,
      choicesIds: finishChoicesIds,
    };

    const newState = {
      ...state,
      [chosenCategory]: {
        ...state[chosenCategory],
        [questionNumber]: {
          ...state[chosenCategory][questionNumber],
          columns: {
            ...state[chosenCategory][questionNumber].columns,
            [newStart.id]: newStart,
            [newFinish.id]: newFinish,
          },
        },
      },
    };
    checkAnswer(
      initialData[chosenCategory][questionNumber].correct,
      result.draggableId,
      setCorrectAnswers
    );
    setAddStyle(true);
    setChosenAnswer(result.draggableId);
    afterSeconds(setQuestionNumber);
    setState(newState);
  };

  const correctAnswerImg = () => {
    switch (chosenCategory) {
      case "technology":
        return correctTechnology;
      case "moto":
        return correctMoto;
      case "history":
        return correctHistory;
      case "culture":
        return correctCulture;
      case "programming":
        return correctProgramming;
      default:
        return "button";
    }
  };

  const result = initialData[chosenCategory][questionNumber];

  return questionNumber === 2 ||
    questionNumber === 6 ||
    questionNumber === 10 ? (
    <>
      <div
        className={
          addStyle === true ? "unclickable wrapper-dnd" : "wrapper-dnd"
        }
      >
        <DragDropContext onDragEnd={onDragEnd}>
          <Result
            initialData={state}
            chosenCategory={chosenCategory}
            questionNumber={questionNumber}
            setQuestionNumber={setQuestionNumber}
            addStyle={addStyle}
            chosenAnswer={chosenAnswer}
            correctAnswerImg={correctAnswerImg}
          />
          <Column
            initialData={state}
            chosenCategory={chosenCategory}
            questionNumber={questionNumber}
            setQuestionNumber={setQuestionNumber}
            addStyle={addStyle}
            chosenAnswer={chosenAnswer}
          />
        </DragDropContext>
      </div>
    </>
  ) : (
    <div className={addStyle === true ? "unclickable answers" : "answers"}>
      {result.choices.map((choice, index) => (
        <div
          key={index}
          className={
            (chosenAnswer === choice ? "correctAnswer" : null,
            "button_" + chosenCategory)
          }
          onClick={() => {
            afterSeconds();
            setChosenAnswer(choice);
            setAddStyle(true);
            checkAnswer(result.correct, choice);
          }}
        >
          <p className="choice">{choice}</p>
          <img
            key={index}
            src={correctAnswerImg()}
            alt="correct"
            className={addStyle && choice === result.correct ? "showImg" : null}
          />
        </div>
      ))}
    </div>
  );
};

export default Answer;
