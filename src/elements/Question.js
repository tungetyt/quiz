import initialData from "../initialData";

const Question = ({ chosenCategory, questionNumber, setScoreSrc }) => {
  const result = initialData[chosenCategory][questionNumber].question;
  return (
    <div className="question-wrapper">
      <h2 className={"chosenCat bar_" + chosenCategory}>
        Wybierz poprawną odpowiedź
      </h2>
      <img src={setScoreSrc()} alt="number of question" className="number" />
      <p className="numberQuestion">
        {questionNumber}/<span>10</span>
      </p>
      <p className="question">
        {questionNumber}. {result}
      </p>
    </div>
  );
};

export default Question;
