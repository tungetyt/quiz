import moto_icon from "../assets/motoryzacja_ikona.svg";

const Moto = ({ chosenCategory, setChosenCategory }) => {
  return (
    <li
      onClick={() => {
        setChosenCategory("moto");
      }}
      className={chosenCategory + "Small"}
    >
      <img src={moto_icon} alt="automotive" />
      <hr />
      <p>Motoryzacja</p>
    </li>
  );
};

export default Moto;
