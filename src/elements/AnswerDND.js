import { Draggable } from "react-beautiful-dnd";

import correct from "../assets/poprawna_odpowiedź_.png";

const AnswerDND = ({
  choice,
  index,
  initialData,
  chosenCategory,
  questionNumber,
  addStyle,
}) => {
  const result = initialData[chosenCategory][questionNumber];

  return (
    <Draggable draggableId={choice} index={index}>
      {(provided) => (
        <div
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          className={"borderless button_" + [chosenCategory]}
        >
          <p className="choice">{choice}</p>
          <img
            src={correct}
            alt="correct"
            className={
              addStyle && choice === result.correct ? "showImg" : "hideImg"
            }
          />
        </div>
      )}
    </Draggable>
  );
};

export default AnswerDND;
