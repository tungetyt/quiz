import culture_icon from "../assets/kultura_ikona.svg";

const Culture = ({ chosenCategory, setChosenCategory }) => {
  return (
    <li
      onClick={() => {
        setChosenCategory("culture");
      }}
      className={chosenCategory + "Small"}
    >
      <img src={culture_icon} alt="culture" />
      <hr />
      <p>Kultura</p>
    </li>
  );
};

export default Culture;
