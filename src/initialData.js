const initialData = {
  technology: {
    1: {
      id: "q1",
      question: "Która z firm jest dziś liderem sprzedaży na rynku PC?",
      choices: ["A. Asus", "B. Lenovo", "C. Acer", "D. MSI", "E. HP"],
      correct: "B. Lenovo",
    },
    2: {
      id: "q2",
      question: "W którym roku miał premierę pierwszy iPhone?",
      choices: ["A. 2006", "B. 2007", "C. 2008", "D. 2009"],
      correct: "B. 2007",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: ["A. 2006", "B. 2007", "C. 2008", "D. 2009"],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    3: {
      id: "q3",
      question: "Która z firm nie jest producentem akcesoriów komputerowych?",
      choices: [
        "A. StellSeries",
        "B. Razer",
        "C. 4Tech",
        "D. Logitech",
        "E. SAP",
      ],
      correct: "E. SAP",
    },
    4: {
      id: "q4",
      question: "Kto był twórcą portu USB?",
      choices: ["A. Microsoft", "B. Apple", "C. AMD", "D. Intel"],
      correct: "D. Intel",
    },
    5: {
      id: "q5",
      question: "Skrót BIOS oznacza:",
      choices: [
        "A. Basic Input Output System",
        "B. Byte Integration Operations System",
        "C. Basic Interconnect Operating System",
        "D. Żadne z powyższych",
      ],
      correct: "A. Basic Input Output System",
    },
    6: {
      id: "q6",
      question: "Kto jest twórcą standardu x86?",
      choices: ["A. IBM", "B. AMD", "C. Acer", "D. Apple", "E. Intel"],
      correct: "E. Intel",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: ["A. IBM", "B. AMD", "C. Acer", "D. Apple", "E. Intel"],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    7: {
      id: "q7",
      question: "Liderem rynku kart graficznych jest:",
      choices: ["A. AMD", "B. Nvidia", "C. Apple", "D. Microsoft", "E. ATI"],
      correct: "B. Nvidia",
    },
    8: {
      id: "q8",
      question: "Która z nazw nie oznacza systemu operacyjnego?",
      choices: ["A. DOS", "B. Linux", "C. OSX", "D. Ubuntu", "E. HyperX"],
      correct: "E. HyperX",
    },
    9: {
      id: "q9",
      question: "W którym roku powstał Facebook?",
      choices: ["A. 2005", "B. 2006", "C. 2002", "D. 2004", "E. 2003"],
      correct: "D. 2004",
    },
    10: {
      id: "q10",
      question: "W którym roku powstała poczta gmail?",
      choices: ["A. 2003", "B. 2004", "C. 2006", "D. 2008", "E. 2005"],
      correct: "B. 2004",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: ["A. 2003", "B. 2004", "C. 2006", "D. 2008", "E. 2005"],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
  },
  culture: {
    1: {
      id: "q1",
      question: "Skąd pochodził Wit Stwosz?",
      choices: [
        "A. Z Polski",
        "B. Z Danii",
        "C. Z Niemiec",
        "D. Z Holandii",
        "E. Z Francji",
      ],
      correct: "C. Z Niemiec",
    },
    2: {
      id: "q2",
      question:
        "Pierwowzorem Pana Młodego w dramacie 'Wesele' Stanisława Wyspiańskiego był",
      choices: [
        "A. Stańczyk",
        "B. Lucjan Rydel",
        "C. Stanisław Przybyszewski",
        "D. Jan Kasprowicz",
        "E. Adam Małysz",
      ],
      correct: "B. Lucjan Rydel",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Stańczyk",
            "B. Lucjan Rydel",
            "C. Stanisław Przybyszewski",
            "D. Jan Kasprowicz",
            "E. Adam Małysz",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    3: {
      id: "q3",
      question: "W którym filmie Andrzeja Wajdy NIE zagrał Zbiegniew Cybulski?",
      choices: [
        "A. Popiół i diament",
        "B. Pokolenie",
        "C. Popioły",
        "D. Ogniem i Mieczem",
      ],
      correct: "C. Popioły",
    },
    4: {
      id: "q4",
      question: "Kim był Witold Gruca?",
      choices: [
        "A. Dyrygentem",
        "B. Aktorem",
        "C. Tancerzem i choreografem",
        "D. Pisarzem",
      ],
      correct: "C. Tancerzem i choreografem",
    },
    5: {
      id: "q5",
      question: "Kto jest autorem słynnej Symfonii pieśni żałosnych:",
      choices: [
        "A. Henryk Mikołaj Górecki",
        "B. Krzysztof Lutosławski",
        "C. Witold Lutosławki",
        "D. Jan Lam",
      ],
      correct: "A. Henryk Mikołaj Górecki",
    },
    6: {
      id: "q6",
      question:
        "Pierwszym laureatem Europejskich Nagród Filmowych był Polak. Który?",
      choices: [
        "A. Andrzej Wajda",
        "B. Krzysztof Kieślowski",
        "C. Jerzy Skolimowski",
        "D. Jacek Adamczak",
      ],
      correct: "B. Krzysztof Kieślowski",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Andrzej Wajda",
            "B. Krzysztof Kieślowski",
            "C. Jerzy Skolimowski",
            "D. Jacek Adamczak",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    7: {
      id: "q7",
      question:
        "Której z wymienionych oper NIE stworzył Wolfgang Amadeus Mozart?",
      choices: [
        "A. Cyrulik sewilski",
        "B. Wesele Figara",
        "C. Czarodziejski flet",
        "D. Don Giovanni",
      ],
      correct: "A. Cyrulik sewilski",
    },
    8: {
      id: "q8",
      question:
        "Jak nazywa się kultowy zespół rockowy, którego albumy to m.in 'Help!, 'Please Please Me'?",
      choices: [
        "A. The Rolling Stones",
        "B. The Beatles",
        "C. Pink Floyd",
        "D. Red Hot Chilli Pepers",
      ],
      correct: "B. The Beatles",
    },
    9: {
      id: "q9",
      question:
        "Most Golden Gate to jeden z najpiękniejszych i najbardziej rozpoznawalnych mostów na świecie. Gdzie się znajduje?",
      choices: [
        "A. W Nowym Jorku",
        "B. Nad Zatoką San Francisco",
        "C. W Nowym Orleanie",
        "D. Na rzece Kolorado",
      ],
      correct: "B. Nad Zatoką San Francisco",
    },
    10: {
      id: "q10",
      question:
        "Międzynarodowy Konkurs Pianistyczny im. Fryderyka Chopina odbywa się:",
      choices: [
        "A. Co pięć lat w Warszawie",
        "B. Co cztery lata w Warszawie",
        "C. Co cztery lata w Żelazowej Woli",
        "D. Co pięć lat w Żelazowej Woli",
      ],
      correct: "A. Co pięć lat w Warszawie",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Co pięć lat w Warszawie",
            "B. Co cztery lata w Warszawie",
            "C. Co cztery lata w Żelazowej Woli",
            "D. Co pięć lat w Żelazowej Woli",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
  },
  moto: {
    1: {
      id: "q1",
      question:
        "Bugatti Chiron to najszybszy samochód seryjny świata. Chiron to:",
      choices: [
        "A. Wiatr wiejący u wybrzeży Normandii",
        "B. Gatunek kalmara, który bardzo szybko się porusza",
        "C. Kierowca wyścigowy Bugatti Louis Chiron",
        "D. Gatunek lamparda",
      ],
      correct: "C. Kierowca wyścigowy Bugatti Louis Chiron",
    },
    2: {
      id: "q2",
      question:
        "Alfa to Anonima Lombarda Fabbrica Automobili. A Romeo pochodzi od:",
      choices: [
        "A. Bohatera sztuki Szekspira",
        "B. Nazwisko inżyniera Nicoli Romea",
        "C. Dzielnicy rozpusty w Mediolanie",
      ],
      correct: "B. Nazwisko inżyniera Nicoli Romea",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Bohatera sztuki Szekspira",
            "B. Nazwisko inżyniera Nicoli Romea",
            "C. Dzielnicy rozpusty w Mediolanie",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    3: {
      id: "q3",
      question: "Podsterowność to:",
      choices: [
        "A. Podatność auta na boczne podmuchy wiatru",
        "B. Nazwa układu znajdującego się pod kierownicą",
        "C. Zjawisko podczas którego pojazd traci przyczepność przedniej osi i wypada przodem na zewnątrz zakrętu",
      ],
      correct:
        "C. Zjawisko podczas którego pojazd traci przyczepność przedniej osi i wypada przodem na zewnątrz zakrętu",
    },
    4: {
      id: "q4",
      question: "Marki Citroen i Peugeot należą do koncernu:",
      choices: [
        "A. FCA",
        "B. PSA",
        "C. Stellantis",
        "D.  Volkswagen Aktiengesellschaft",
      ],
      correct: "C. Stellantis",
    },
    5: {
      id: "q5",
      question: "Japońska marka Mitsubishi należy do tego samego aliansu co:",
      choices: ["A. Honda", "B. Nissan", "C. Toyota", "D. Skoda"],
      correct: "B. Nissan",
    },
    6: {
      id: "q6",
      question: "Partnerem Mazdy jest:",
      choices: ["A. Ford", "B. Suzuki", "C. Toyota", "D. Fiat"],
      correct: "C. Toyota",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: ["A. Ford", "B. Suzuki", "C. Toyota", "D. Fiat"],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    7: {
      id: "q7",
      question: "100-procentowym właścicielem Bugatti jest:",
      choices: ["A. Bentley", "B. Fiat", "C. Ferrari", "D. Volkswagen"],
      correct: "D. Volkswagen",
    },
    8: {
      id: "q8",
      question: "Nazwa samochoów Mercedes wzięła się od:",
      choices: [
        "A. Imienia córki Emila Jellinka",
        "B. Imienia teściowej Gottlieba Daimlera",
        "C. Imienia żony Karla Benza",
        "D. Imienia kochanki Karla Benza",
      ],
      correct: "A. Imienia córki Emila Jellinka",
    },
    9: {
      id: "q9",
      question: "BMW to Bayerische Motoren Werke. Czy Bayerische to:",
      choices: [
        "A. Bajerancka",
        "B. Bawarska",
        "C. Brandeburska",
        "D. Precyzyjna",
      ],
      correct: "B. Bawarska",
    },
    10: {
      id: "q10",
      question: "Nazwa Mitsubishi oznacza po japońsku:",
      choices: [
        "A. Trzy Perły",
        "B. Trzy Plejady",
        "C. Trzy Diamenty",
        "D. Cztery koła",
      ],
      correct: "C. Trzy Diamenty",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Trzy Perły",
            "B. Trzy Plejady",
            "C. Trzy Diamenty",
            "D. Cztery koła",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
  },
  programming: {
    1: {
      id: "q1",
      question: "Czym jest inkrementacja:",
      choices: [
        "A. Zmniejszenie wartości o 1",
        "B. Zwiększenie wartości o n, gdzie n>=2",
        "C. Zwiększenie wartości o 1",
        "D. Dodanie dowolnej wartości (n)",
      ],
      correct: "C. Zwiększenie wartości o 1",
    },
    2: {
      id: "q2",
      question:
        "Do czego odnoszą się modyfikatory public, private i protected:",
      choices: [
        "A. Dziedziczenia",
        "B. Przestrzeni nazw",
        "C. Polimorfizmu",
        "D. Zabezpieczania danych",
      ],
      correct: "A. Dziedziczenia",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Dziedziczenia",
            "B. Przestrzeni nazw",
            "C. Polimorfizmu",
            "D. Zabezpieczania danych",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    3: {
      id: "q3",
      question: "Tzw. notacja dużego O używana jest do:",
      choices: [
        "A. Opisu złożoności obliczeniowej algorytmu",
        "B. Opisu ilości wątków używanych przez program",
        "C. Opisu wymagań systemowych do uruchomienia programu",
        "D. Opisu klas i psudoklas w CSS",
      ],
      correct: "A. Opisu złożoności obliczeniowej algorytmu",
    },
    4: {
      id: "q4",
      question:
        "Co oznacza znak podwójnej równości == w językach pochodnych od języka C:",
      choices: [
        "A. Przypisania wartości zmiennej",
        "B. Porównania wartości zmiennych",
        "C. Porównania typu wartości",
        "D. Żadne z powyższych",
      ],
      correct: "B. Porównania wartości zmiennych",
    },
    5: {
      id: "q5",
      question: "Proces inicjalizacji zmiennej to:",
      choices: [
        "A. Przypisanie wartości początkowej zmiennej",
        "B. Utworzenie drugiej zmiennej, o takiej samej nazwie co pierwsza",
        "C. Żadne z powyższych",
        "D. Utworzenie drugiej zmiennej, o takiej samej wartości co pierwsza",
      ],
      correct: "A. Przypisanie wartości początkowej zmiennej",
    },
    6: {
      id: "q6",
      question: "Czym jest tzw. IDE:",
      choices: [
        "A. Algorytm",
        "B. Środowisko programistyczne",
        "C. Biblioteka przydatnych funkcji",
        "D. Biblioteka JavaScriptowa",
      ],
      correct: "B. Środowisko programistyczne",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Algorytm",
            "B. Środowisko programistyczne",
            "C. Biblioteka przydatnych funkcji",
            "D. Biblioteka JavaScriptowa",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    7: {
      id: "q7",
      question: "Od jakiej liczby zaczyna się indeksacja tablic:",
      choices: ["A. 0", "B. 1", "C. 2", "D. -1"],
      correct: "A. 0",
    },
    8: {
      id: "q8",
      question:
        "Zakładając, że w danym języku programowania indeksacja tablicy rozpoczyna się od 5, ostatni indeks w tablicy dziesięcio elementowej będzie wynosił:",
      choices: ["A. 16", "B. 15", "C. 14", "D. 13"],
      correct: "C. 14",
    },
    9: {
      id: "q9",
      question: "Kompilator to:",
      choices: [
        "A. Urządzenie używane do weryfikacji poprawności kodu programu",
        "B. Program umożliwiający sprawne wyszukanie błędów w kodzie",
        "C. Program tłumaczący kod źródłowy programu na kod maszynowy",
        "D. Żadne z powyższych",
      ],
      correct: "C. Program tłumaczący kod źródłowy programu na kod maszynowy",
    },
    10: {
      id: "q10",
      question: "Strumienie w programowaniu używane są do:",
      choices: [
        "A. Wprowadzania wartości do programu",
        "B. Szybkiej analizy działania programu",
        "C. Określania pamięci programu",
        "D. Żadne z powyższych",
      ],
      correct: "A. Wprowadzania wartości do programu",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Wprowadzania wartości do programu",
            "B. Szybkiej analizy działania programu",
            "C. Określania pamięci programu",
            "D. Żadne z powyższych",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
  },
  history: {
    1: {
      id: "q1",
      question: "Katarzyna II Wielka była cesarzową:",
      choices: ["A. Rosji", "B. Prus", "C. Węgier", "D. Austrii"],
      correct: "A. Rosji",
    },
    2: {
      id: "q2",
      question:
        "Która z bitew kampanii wrześniowej została nazwana polskimi Termopilami?",
      choices: [
        "A. Bitwa pod Krasnymstawem",
        "B. Obrona Przemyśla",
        "C. Obrona Wizny",
        "D. Szarża pod Wólką Węglową",
      ],
      correct: "C. Obrona Wizny",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Bitwa pod Krasnymstawem",
            "B. Obrona Przemyśla",
            "C. Obrona Wizny",
            "D. Szarża pod Wólką Węglową",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    3: {
      id: "q3",
      question: "W którym wieku żył Gall Anonim?",
      choices: ["A. W XVI w.", "B. W IX w.", "C. W XII w.", "D. W XV w."],
      correct: "C. W XII w.",
    },
    4: {
      id: "q4",
      question:
        "Który z poniżej wymienionych był ostatnim niemieckim cesarzem?",
      choices: [
        "A. Wilhelm II Hohenzollern",
        "B. Fryderyk Wilhelm IV Hohenzollern",
        "C. Fryderyk III Hohenzollern",
        "D. Wilhelm I Hohenzollern",
      ],
      correct: "A. Wilhelm II Hohenzollern",
    },
    5: {
      id: "q5",
      question: "O co spierali się Grzegorz VII i niemiecki cesarz Henryk IV?",
      choices: [
        "A. O przywileje dla chłopów",
        "B. O prawo do inwestytury duchownych",
        "C. O budowę nowej świątyni",
        "D. O krucjaty dziecięce",
      ],
      correct: "B. O prawo do inwestytury duchownych",
    },
    6: {
      id: "q6",
      question: "Z którymi zwierzętami Hannibal przeprawił się przez Alpy?",
      choices: ["A. Lwami", "B. Słoniami", "C. Tygrysami", "D. Wielbłądami"],
      correct: "B. Słoniami",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Lwami",
            "B. Słoniami",
            "C. Tygrysami",
            "D. Wielbłądami",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
    7: {
      id: "q7",
      question: "Gdzie Martin Luter ogłosił swoję tezy?",
      choices: [
        "A. W Norymberdze",
        "B. W Wittenberdze",
        "C. W Monachium",
        "D. W Akwizgranie",
      ],
      correct: "B. W Wittenberdze",
    },
    8: {
      id: "q8",
      question: "W którym państwie władzę sprawował Francisco Franco?",
      choices: [
        "A. W Hiszpanii",
        "B. W Portugalii",
        "C. We Francji",
        "D. We Włoszech",
      ],
      correct: "A. W Hiszpanii",
    },
    9: {
      id: "q9",
      question: "W którym roku powstało Księstwo Warszawskie?",
      choices: ["A. W 1807", "B. W 1896", "C. W 1900", "D. W 1859"],
      correct: "A. W 1807",
    },
    10: {
      id: "q10",
      question: "Którą bitwą zakończyło się tzw. 100 dni Napoleona?",
      choices: [
        "A. Bitwą Narodów",
        "B. Bitwą pod Borodino",
        "C. Bitwą pod Waterloo",
        "D. Bitwą nad Berezyną",
      ],
      correct: "C. Bitwą pod Waterloo",
      columns: {
        "column-1": {
          id: "column-1",
          choicesIds: [
            "A. Bitwą Narodów",
            "B. Bitwą pod Borodino",
            "C. Bitwą pod Waterloo",
            "D. Bitwą nad Berezyną",
          ],
        },
        "column-2": {
          id: "column-2",
          choicesIds: [],
        },
      },
    },
  },
};

export default initialData;
