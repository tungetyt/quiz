import { DragDropContext } from "react-beautiful-dnd";
import Column from "../elements/Column";
import initialData from "../initialData";
import { useState } from "react";
import Result from "../elements/Result";

const OptionDND = (props) => {
  const [state, setState] = useState(initialData);

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    const start =
      state[props.chosenCategory][props.questionNumber].columns[
        source.droppableId
      ];
    const finish =
      state[props.chosenCategory][props.questionNumber].columns[
        destination.droppableId
      ];

    if (start === finish) {
      const newChoicesIds = Array.from(start.choicesIds);
      newChoicesIds.splice(source.index, 1);
      newChoicesIds.splice(destination.index, 0, draggableId);

      const newColumn = {
        ...start,
        choicesIds: newChoicesIds,
      };

      const newState = {
        ...state,
        [props.chosenCategory]: {
          ...state[props.chosenCategory],
          [props.questionNumber]: {
            ...state[props.chosenCategory][props.questionNumber],
            columns: {
              ...state[props.chosenCategory][props.questionNumber].columns,
              [newColumn.id]: newColumn,
            },
          },
        },
      };

      console.log(newState);

      setState(newState);
      return;
    }

    //Moving from one list to another

    const startChoicesIds = Array.from(start.choicesIds);
    startChoicesIds.splice(source.index, 1);

    const newStart = {
      ...start,
      choicesIds: startChoicesIds,
    };

    const finishChoicesIds = Array.from(finish.choicesIds);
    finishChoicesIds.splice(destination.index, 0, draggableId);

    const newFinish = {
      ...finish,
      choicesIds: finishChoicesIds,
    };

    const newState = {
      ...state,
      [props.chosenCategory]: {
        ...state[props.chosenCategory],
        [props.questionNumber]: {
          ...state[props.chosenCategory][props.questionNumber],
          columns: {
            ...state[props.chosenCategory][props.questionNumber].columns,
            [newStart.id]: newStart,
            [newFinish.id]: newFinish,
          },
        },
      },
    };

    setState(newState);
  };

  return (
    <div className="wrapper-dnd">
      {state[props.chosenCategory][props.questionNumber].columns['column-2'].choicesIds.length === 1 ? props.setQuestionNumber(prevState => prevState + 1) : null}
      <DragDropContext onDragEnd={onDragEnd}>
        <Result
          initialData={state}
          chosenCategory={props.chosenCategory}
          questionNumber={props.questionNumber}
          setQuestionNumber={props.setQuestionNumber}
        />
        <Column
          initialData={state}
          chosenCategory={props.chosenCategory}
          questionNumber={props.questionNumber}
          setCorrectAnswers={props.setCorrectAnswers}
          setButton={props.setButton}
        />
      </DragDropContext>
    </div>
  );
};

export default OptionDND;
