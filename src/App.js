import "./App.css";
import "./styles/themes/default/theme.scss";
import Homepage from "./pages/homepage";

function App() {
  return (
      <Homepage />
  );
}

export default App;
